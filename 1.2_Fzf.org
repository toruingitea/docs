:PROPERTIES:
:ID:       213eb3dc-28f3-4433-8b72-8bd32300ceec
:END:
#+title: Fzf
#+description: fuzzy finder
#+author: toru
#+category: nota
#+startup: showeverything
#+date: 2023-11-24 03:53:33 AM

* Table :toc:
- [[#atomic][Atomic]]
- [[#wiki][Wiki]]
  - [[#others][Others]]
  - [[#flags][Flags]]
  - [[#examples][Examples]]

* Atomic
fuzzy finder program

* Wiki

** Others
- killall -18 <TAB> :: (kill procesess)
- cat "FILE" | fzf -f <string> :: search in file
  - cat "FILE" | fzf -f "'<string>" :: search that word

** Flags
-i	All queries are case-insensitive.
+i	All queries are case-sensitive.

--reverse 
        Display from the top of the screen 

--height N% 
        Use only N% height instead of full screen. 
        
--query PARAM (--query=.md$)
        Begin search with PARAM as the initial query 
- .xml$ | .yml$ | .tex$

--multi 
        Enable multi-select with tab/shift-tab. 

-f, --filter PARAM (.org)
        Show matches for PARAM without the interactive finder

** Examples
- alias pacr :: "pacman -Qq | fzf --multi --preview 'pacman -Qi {2}' | xargs -ro sudo pacman -Rncs"

  - el pacman -Qq es para que muestre todos los paquetes ya instalados

  - el fzf multi es para que puedas agarrar varios

  - preview es para que se muestre a la derecha "pacman -Qi" que es la descripción del paquete, ¿Cuál paquete? el {1} que es el que estamos seleccionando

  - xargs -ro para pasarselo a "sudo pacman -Rns" que lo va a desinstalar con todo y dependencias que no sean las dependencias de otros programas
