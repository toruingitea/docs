:PROPERTIES:
:ID:       141cd382-dbd9-47e4-9cb4-889531249dfc
:END:
#+title: gpg
#+description: best privacy-security
#+author: toru
#+category: nota
#+startup: showeverything
#+date: 2023-11-23 03:53:38 PM

* Table :toc:
- [[#atomic][Atomic]]
- [[#wiki][Wiki]]
  - [[#gpg-key][gpg key]]
- [[#migration][migration]]
  - [[#basic-encryptiondecryption][basic encryption/decryption]]
  - [[#with-tar][with tar]]
  - [[#trust-key][trust key]]

* Atomic

* Wiki

** gpg key
- gpg --full-gen-key (generate public & private key)
- gpg -k (recover gpg key)
- gpg --list-secret-keys --keyid-format LONG (getting you gpg key)
- gpg --list-keys
- gpg --armor --export "mail" > pubkey.asc (public key)
- gpg --armor --export-secret-keys "mail" > privkey.asc (pivate key)
- gpg --export-secret-key -a toru1 >> prv.key
- gpg --export -a toru1 > pub.key

* migration
- gpg --import public.key
- gpg --import private.key

** basic encryption/decryption
- gpg --encrypt --recipient "mail" "file to encrypt"
- gpg -r "file" -e "plain.gpg" (encrypt the file)
- gpg --decrypt "file to decrypt"
- gpg -d "file.gpg" (outputs the content onto the terminal)
- gpg -d "file.gpg" >> file (to see the entire content)
- gpg --gen-random --armor 1 21 (genera una contraseña random de 21 caracteres)

** with tar
- tar -czvf "gpg-backups.tar.gz" "gpg-backups" (directory) tar -xf "gpg-backups.tar.gz"

** trust key
- gpg --edit-key "mail" gpg> trust 5
