:PROPERTIES:
:ID:       4a27dc94-8084-4cda-b437-bf76af588c2e
:END:
#+title: X utils
#+description:
#+author: toru
#+category: nota
#+startup: showeverything
#+date: 2023-11-24 06:38:31 AM

* Table :toc:
- [[#wiki][Wiki]]
  - [[#xkill][xkill]]
  - [[#xset][xset]]

* Wiki
** xkill
** xset
- xset -dpms
- xset +dpms :: volver a activarlo
- xset s off :: quitarlo
- xset s 3600 (seconds) :: quitarlo luego de cierto tiempo
